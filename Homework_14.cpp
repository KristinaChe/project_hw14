#include <iostream>
#include <string>

int main()
{
    //���������� � ������������� ��������� ����������
    std::string text{ "Skillbox is awesome!" };

    //����� ���������� �� �����
    std::cout << text << "\n";

    //����� ����� ������ ����������
    std::cout << text.length() << "\n";

    //����� ������� �������� ������
    std::cout << text.front() << "\n";

    //����� ���������� �������� ������
    std::cout << text.back() << std::endl;
}
